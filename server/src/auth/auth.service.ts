import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from 'src/users/dto/user.dto';
import { UserLogin } from 'src/users/dto/userLogin.dto';
import { JwtPayload } from './jwt.payload';
import { RegistrationStatus } from './auth.registrationStatus';
import { LoginStatus } from './auth.loginStatus';
import * as bcrypt from 'bcryptjs'

@Injectable()
export class AuthService {
    constructor(private readonly usersService: UsersService, private readonly jwtService: JwtService) {}

    async register(userDto: UserDto): Promise<RegistrationStatus> {
        let status: RegistrationStatus = {
            success: true,
            message: 'user registered'
        }

        try {
            await this.usersService.createUser(userDto)
        } catch (e) {
            status = {
                success: false,
                message: e
            }
        }

        return status
    }

    async login(userLoginDto: UserLogin): Promise<LoginStatus> {
        const user = await this.usersService.findByEmail(userLoginDto)

        if (!await bcrypt.compare(userLoginDto.password, user.password)) {
            throw new HttpException('Invalid credentials', 400)
        }

        const token = this._createToken(user)

        return {
            email: user.email, ...token
        }
    }

    async validateUser(payload: JwtPayload): Promise<UserDto> {
        const user = await this.usersService.findByPayload(payload)

        if (!user) {
            throw new HttpException('Invalid token', HttpStatus.UNAUTHORIZED)
        }
        return user
    }

    private _createToken({ email }: UserDto): any {
        const user: JwtPayload = { email }
        const accessToken = this.jwtService.sign(user)

        return {
            expiresIn: 600000,
            accessToken
        }
    }
}

