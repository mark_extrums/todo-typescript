import { Module } from '@nestjs/common';
import { UsersModule } from 'src/users/users.module';
import { PassportModule } from '@nestjs/passport'
import { JwtModule } from '@nestjs/jwt'
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { JwtStrategy } from './jwt.strategy';
import { ConfigService } from '@nestjs/config';

@Module({
    imports: [
        UsersModule,
        PassportModule.register({
            defaultStrategy: 'jwt',
            property: 'user',
            session: false
        }),
        JwtModule.register({
            secret: 'SomeSecret',
            signOptions: {
                expiresIn: 600000
            }
        })
    ],
    providers: [AuthService, JwtStrategy, ConfigService],
    controllers: [AuthController],
    exports: [PassportModule]
})
export class AuthModule {}
