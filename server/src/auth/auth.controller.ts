import { Controller, Post, Body, HttpException, HttpStatus, Get, UseGuards } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserDto } from 'src/users/dto/user.dto';
import { RegistrationStatus } from './auth.registrationStatus';
import { UserLogin } from 'src/users/dto/userLogin.dto';
import { LoginStatus } from './auth.loginStatus';
import { AuthGuard } from '@nestjs/passport';
import { ConfigService } from '@nestjs/config';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService, private readonly configService: ConfigService) {}

    @Post('register')
    public async register(@Body() userDto: UserDto): Promise<RegistrationStatus> {
        const result: RegistrationStatus = await this.authService.register(userDto)

        if (!result.success) {
            throw new HttpException(result.message, HttpStatus.BAD_REQUEST)
        }

        return result
    }

    @Post('login')
    public async login(@Body() userLoginDto: UserLogin): Promise<LoginStatus> {
        return await this.authService.login(userLoginDto)
    }

    @Get()
    @UseGuards(AuthGuard())
    public isAuth(): any {
        return
    }
}
