export interface LoginStatus {
    accessToken: string,
    expiresIn: number,
    email: string
}