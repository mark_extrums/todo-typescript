import { Injectable, Body, Req, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './entity/task.entity';
import { TaskCreate } from './dto/taskCreate.dto'
import { Repository } from 'typeorm';
import { DeleteStatus } from './tasks.deleteStatus';

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(Task)
        private taskRepository: Repository<Task>
    ) {}

    async createTask(taskCreateDto: TaskCreate, user: any): Promise<Task> {
        const task = this.taskRepository.create({ ...taskCreateDto, userId: user.id })

        try {
            await task.save()
        } catch (e) {
            throw new BadRequestException()
        }

        return task
    }

    async getTasks(user: any): Promise<Task[]> {
        return await this.taskRepository.find({ userId: user.id })
    }

    async deleteTask(id: number): Promise<DeleteStatus> {
        const deleteStatus: DeleteStatus = { status: true }

        try {
            await this.taskRepository.delete({ id })
            return deleteStatus
        } catch (e) {
            throw new BadRequestException()
        }
    }
}
