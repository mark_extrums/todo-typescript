import { Controller, Post, UseGuards, Req, Get, Body, Delete, Param, ParseIntPipe } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport'
import { TasksService } from './tasks.service';
import { TaskCreate } from './dto/taskCreate.dto';
import { Task } from './entity/task.entity';
import { DeleteStatus } from './tasks.deleteStatus';

@Controller('tasks')
export class TasksController {
    constructor(
        private tasksService: TasksService
    ) {}

    @Post()
    @UseGuards(AuthGuard())
    async createTask(@Req() { user }: any, @Body() taskCreateDto: TaskCreate): Promise<Task> {
        return await this.tasksService.createTask(taskCreateDto, user)
    }

    @Get()
    @UseGuards(AuthGuard())
    async getTasks(@Req() { user }: any): Promise<Task[]> {
        return this.tasksService.getTasks(user)
    }

    @Delete('/:id')
    @UseGuards(AuthGuard())
    async deleteTask(@Param('id', ParseIntPipe) id: number): Promise<DeleteStatus> {
        return this.tasksService.deleteTask(id)
    }
}
