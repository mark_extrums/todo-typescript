import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from 'src/users/users.module';
import { AuthModule } from 'src/auth/auth.module';
import { TasksController } from './tasks.controller'
import { Task } from './entity/task.entity';
import { TasksService } from './tasks.service';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            Task
        ]),
        UsersModule,
        AuthModule
    ],
    controllers: [TasksController],
    providers: [TasksService]
})
export class TasksModule {}
