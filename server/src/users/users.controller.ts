import { Controller, Post, Body } from '@nestjs/common';
import { UsersService } from './users.service';
import { UserDto } from './dto/user.dto';
import { ConfigService } from '@nestjs/config';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService, private configService: ConfigService) {}

    @Post()
    async createUser(@Body() userDto: UserDto) {
        return this.usersService.createUser(userDto)
    }
}
