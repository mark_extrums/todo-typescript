import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './entity/user.entity';
import { UserDto } from './dto/user.dto';
import { UserRepository } from './entity/user.repository';
import { UserLogin } from './dto/userLogin.dto';
import { JwtPayload } from 'src/auth/jwt.payload';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
    ) {}

    createUser(userDto: UserDto): Promise<User> {
        return this.userRepository.createOne(userDto)
    }

    async findByEmail(userLoginDto: UserLogin): Promise<User> {
        const user = await this.userRepository.findOne({ email: userLoginDto.email })

        if (!user) {
            throw new HttpException('Invalid credentials', 400)
        }

        return user
    }

    async findByPayload({ email }: JwtPayload): Promise<User> {
        return await this.userRepository.findOne({ email })
    }
}
