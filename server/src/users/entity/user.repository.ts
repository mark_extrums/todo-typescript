import { EntityRepository, Repository } from "typeorm";
import { User } from "./user.entity";
import { UserDto } from "../dto/user.dto";
import * as bcrypt from 'bcryptjs'
import { BadRequestException } from "@nestjs/common";

@EntityRepository(User)
export class UserRepository extends Repository<User> {
    private async fillObject(object: User, userDto: UserDto) {
        for (let key in userDto) {
            if (key === 'password') {               
                object[key] = await bcrypt.hash(userDto[key], 8)
            } else {
                object[key] = userDto[key]
            }
        }
    }

    async createOne(userDto: UserDto): Promise<User> {
        const user = new User()
        await this.fillObject(user, userDto)
        
        try {
            await user.save()
        } catch (e) {
            throw new BadRequestException()
        }

        return user
    }
}