import * as _ from 'lodash'

interface putAuthInterface {
    type: string,
    payload: boolean
}

export const putAuth = (isAuth: boolean) => {
    return {
        type: 'PUT_AUTH',
        payload: isAuth
    }
}

export const logout = () => {
    localStorage.removeItem('jwt')

    return {
        type: 'LOGOUT'
    }
}

export const getAuth = () => {
    return {
        type: 'GET_AUTH'
    }
}

export const putItems = (items: object[]) => {
    const parsedItems: object = _.mapKeys(items, 'id')

    return {
        type: 'PUT_ITEMS',
        payload: parsedItems
    }
}

export const loadItems = () => {
    return {
        type: 'LOAD_ITEMS'
    }
}

export const putItem = (item: object) => {
    return {
        type: 'PUT_ITEM',
        payload: item
    }
}

export const deleteItem = (id: number) => {
    return {
        type: 'DELETE_ITEM',
        payload: id
    }
}
