import { takeEvery, put, call, fork, spawn, all } from 'redux-saga/effects'
import { isAuthApi, addItemApi, loadItemsApi, deleteItemApi } from '../api/api'
import { putAuth, putItems } from '../actions'

function* workerLoadAuth() {
    try {
        const data = yield call(isAuthApi)
        yield put(putAuth(true))
    } catch (e) {
        yield put(putAuth(false))
    }
}

function* workerLoadItems() {
    const res = yield call(loadItemsApi)
    yield put(putItems(res.data))
}

function* workerDeleteItem(action:any) {
    const res = yield call(deleteItemApi, action.payload)
}

export default function* root() {
    yield all ([
        yield takeEvery('GET_AUTH', workerLoadAuth),
        yield takeEvery('LOAD_ITEMS', workerLoadItems),
        yield takeEvery('DELETE_ITEM', workerDeleteItem)
    ])
}