import * as _ from 'lodash'

const itemsReducer = (state: object = { }, action: any): object => {
    switch(action.type) {
        case 'PUT_ITEMS':
            return { ...action.payload }
        case 'PUT_ITEM':
            return {...state, [action.payload.id]: action.payload}
        case 'DELETE_ITEM':
            return _.omit(state, action.payload)
        default:
            return state
    }
}

export default itemsReducer