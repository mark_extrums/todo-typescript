interface authState {
    isAuth: any
}

interface authAction {
    type: string,
    payload: boolean
}

const authReducer = (state: authState = { isAuth: null }, action: authAction): authState => {
    switch (action.type) {
        case 'PUT_AUTH':
            return { isAuth: action.payload }
        case 'LOGOUT': 
            return { isAuth: false }
        default:
            return state
    }
}

export default authReducer