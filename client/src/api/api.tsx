import Axios from "axios"

const baseURL = 'http://localhost:4000'

export const isAuthApi = () => {
    return Axios({
        url: `${baseURL}/auth`,
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}

export const registerApi = async (email: string, password: string) => {
    return await Axios({
        url: `${baseURL}/auth/register`,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            email,
            password
        }
    })
}

export const loginApi = async (email: string, password: string) => {
    return await Axios({
        url: `${baseURL}/auth/login`,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        data: {
            email,
            password
        }
    }) 
}

export const addItemApi = async (text: string) => {
    return await Axios({
        url: `${baseURL}/tasks`,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        },
        data: {
            text
        }
    })
}

export const loadItemsApi = () => {
    return Axios({
        url: `${baseURL}/tasks`,
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}

export const deleteItemApi = async (id: number) => {
    return await Axios({
        url: `${baseURL}/tasks/${id}`,
        method: 'DELETE',
        headers: {
            'Authorization': `Bearer ${localStorage.getItem('jwt')}`
        }
    })
}