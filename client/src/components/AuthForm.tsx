import React from 'react'
import { Formik, Form, Field } from 'formik'
import { Link } from 'react-router-dom'

export const AuthForm = (props: any) => {
    const renderLink = () => {
        return props.header === 'Login' ? 'Register' : 'Login'
    }

    return (
        <div style={{ width: '80%', margin: 'auto' }}>
            <h4 style={{ textAlign: 'center' }}>{props.header}</h4>
            <Formik
                initialValues={{
                    email: '',
                    password: ''
                }}
                onSubmit={props.onFormSubmit}
                validationSchema={props.validationSchema}
            >
                {({ errors, touched }) => (
                    <Form>
                        <label htmlFor="email">Email</label>
                        <Field id="email" name="email" type="email" />
                        {errors.email && touched.email ? (
                            <div>{errors.email}</div>
                        ) : null}
                        <label htmlFor="password">Password</label>
                        <Field id="password" name="password" type="password" />
                        {errors.password && touched.password ? (
                            <div>{errors.password}</div>
                        ) : null}
                        <button type="submit">{props.header}</button>
                        <Link to={renderLink().toLowerCase()}>{renderLink()}</Link>
                    </Form>
                )}
                
            </Formik>
        </div>
    )
}