import React from 'react'
import ListForm from './ListForm'
import { connect } from 'react-redux'
import Item from './Item'
import { useHistory } from 'react-router-dom'

const List = (props: any) => {
    const history = useHistory()

    const renderList = () => {
        const { items } = props
        const out = []

        for (let key in items) {
            out.push(<Item id={key} key={key} text={items[key].text} />)
        }

        return out
    }

    return (
        <div>
            <ListForm />
            {renderList()}
            {!props.auth && props.auth !== null && history.push('/login')}
        </div>
    )
}

const mapStateToProps = (state: any) => {
    return {
        items: state.items,
        auth: state.auth.isAuth
    }
}

export default connect(mapStateToProps)(List)