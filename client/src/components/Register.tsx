import React, { useEffect } from 'react'
import { AuthForm } from "./AuthForm"
import { FormikHelpers } from 'formik'
import * as Yup from 'yup'
import { registerApi } from '../api/api'
import { useHistory } from 'react-router-dom'

interface Values {
    email: string,
    password: string
}

export const Register = () => {
    const history = useHistory()

    const onFormSubmit = async (values: Values, { setSubmitting, setFieldError }: FormikHelpers<Values>) => {
        const { email, password } = values
        
        try {
            setSubmitting(false)
            const res = await registerApi(email, password)

            history.push('/login')
        } catch (e) {
            setFieldError('email', 'Email is already in use')
        }
    }

    const RegisterValidationSchema = Yup.object().shape({
        email: Yup.string()
            .required('Email is required')
            .email('Invalid email'),
        password: Yup.string()
            .required('Password is required')
            .min(8, 'Password should be >= 8 symbols')
            
    })

    return (
        <AuthForm onFormSubmit={onFormSubmit} header="Register" validationSchema={RegisterValidationSchema} />
    )
}