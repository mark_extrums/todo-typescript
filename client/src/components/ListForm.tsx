import React from 'react'
import { Formik, Form, Field } from 'formik'
import { addItemApi } from '../api/api'
import { useDispatch } from 'react-redux'
import { putItem } from '../actions'

const ListForm = () => {
    const dispatch = useDispatch()

    return (
        <Formik
            onSubmit={async (values) => {
                const item:any = await addItemApi(values.text)
                console.log(item.data)
                dispatch(putItem(item.data))   
            }}
            initialValues={{
                text: ''
            }}
        >
            <Form style={{marginTop:'8px'}}>
                <label htmlFor="text">Enter your task</label>
                <Field id="text" name="text" requiered="true" />
                <button type="submit">Add</button>
            </Form>
        </Formik>
    )
}

export default ListForm