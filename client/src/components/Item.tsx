import './Item.css'
import React from 'react'
import { useDispatch } from 'react-redux'
import { deleteItemApi } from '../api/api'
import { deleteItem } from '../actions'

const Item = (props: any) => {
    const dispatch = useDispatch()

    const deleteHandler = async () => {
        const res = await deleteItemApi(props.id)
        dispatch(deleteItem(props.id))
    }

    return (
        <div className="item-block">
            <div>{props.text}</div>
            <button onClick={deleteHandler}>Delete</button>
        </div>
    )
}

export default Item
