import './Header.css'
import React, { useState, useEffect } from 'react'
import { connect, useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { logout } from '../actions'

const Header = (props: any) => {
    const dispatch = useDispatch()

    const renderLoginButton = () => {
        if (props.auth) {
            return (
                <button onClick={() => dispatch(logout())}>Logout</button>
            )
        } else {
            return <button><Link to="/login">Login</Link></button>
        }
    }

    return (
        <div className="header">
            <h3><Link to="/">Todo</Link></h3>
            {renderLoginButton()}
        </div>
    )
}

const mapStateToProps = (state: any) => {
    
    
    return {
        auth: state.auth.isAuth
    }
}

export default connect(mapStateToProps, {  })(Header)

