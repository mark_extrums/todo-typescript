import React, { useEffect } from 'react';
import Header from './Header';
import { useDispatch } from 'react-redux';
import { getAuth, loadItems } from '../actions';
import { Router, Route, Switch, withRouter, BrowserRouter } from 'react-router-dom';
import { Register } from './Register';
import { Login } from './Login';
import List from './List';

const App: React.FC = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getAuth())
    dispatch(loadItems())
  })

  return (
    <div style={{ width: '80%', margin: 'auto' }}>
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" exact component={List} />
          <Route path="/register" exact component={Register} />
          <Route path="/login" exact component={Login} />
        </Switch>

      </BrowserRouter>
      
    </div>
  )
}

export default App;
