import React from 'react'
import { AuthForm } from './AuthForm'
import { FormikHelpers } from 'formik'
import { loginApi } from '../api/api'
import { useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { putAuth } from '../actions'

interface Values {
    email: string,
    password: string
}


export const Login = (props: any) => {
    const history = useHistory()
    const dispatch = useDispatch()

    const onFormSubmit = async (values: Values, { setSubmitting, setFieldError }: FormikHelpers<Values>) => {
        const { email, password } = values
        
        try {
            setSubmitting(false)
            const res = await loginApi(email, password)

            localStorage.setItem('jwt', res.data.accessToken)
            dispatch(putAuth(true))

            history.push('/')
        } catch (e) {
            setFieldError('email', 'Invalid credentials')
            setFieldError('password', 'Invalid credentials')
        }
    }

    return (
        <AuthForm onFormSubmit={onFormSubmit} header="Login" />
    )
}
